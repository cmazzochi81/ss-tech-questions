<?php

namespace App\Http\Controllers;

use PaymentRepository;
use Illuminate\Http\JsonResponse;

class PaymentController extends Controller
{
    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    public function index(Request $request): JsonResponse
    {
        $payments = $this->paymentRepository->getAll($request->all());
        return new JsonResponse(['payments' => $payments]);
    }
}
